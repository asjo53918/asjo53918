#Introduction to IT Security

#Learning objectives

##Knowledge
The student has knowledge and understanding of:

- Basic programming principles
- Basic network protocols
- Security level in the most used network protocols

#Skills
The student can:

- Use primitive data types and abstract data types
- Construct simple programs that use SQL databases
- Construct simple programs that can use networks
- Construct and use tools for example to intercept and filter network traffic
- Set up a simple network.
- Master various network analysis tools
- Read other people's scripts and review and change them

#Skills
The student can:

- Handle minor scripting programs from an IT security perspective

#ECTS scope
The subject element Introduction to IT security has a scope of 5 ECTS points.