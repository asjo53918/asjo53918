#Network and communication security

#Learning Objectives

##Knowledge
The student has knowledge and understanding of:

- Network threats
- Wireless security
- Security in TCP/IP
- Addressing in the different layers
- In-depth knowledge of several of the most used internet protocols (ssl)
- Which devices use which protocols
- Different sniffing strategies and techniques
- Network management (monitoring/logging, snmp)
- Different VPN setups
- Common network devices used in connection with security (firewall, IDS/IPS, honeypot, DPI)

##Skills
The student can:

- Monitor networks and network components, (eg IDS or IPS, honeypot)
- Test networks for attacks targeting the most commonly used protocols
- Identify vulnerabilities that a network may have.

##Skills
The student can handle development-oriented situations including:

- Design, construct and implement as well as test a secure network
- Monitor and manage a network's components
- Prepare a report on the vulnerabilities a network might have (red team report)
- Set up and configure an IDS or IPS


#Examination
ECTS:
The scope of the exam is 10 ECTS.
Sample form:
The test is an individual, oral test based on a question that the student chooses for the exam.
All questions that can be drawn for the exam have been handed out to the students no later than 14
days before the exam, so that the students have the opportunity to prepare.
Regarding
the questions, emphasis will be placed on the student being able to include examples from
the project work and practical exercises from the previous semester.
There is no preparation on the day itself.
The individual, oral exam lasts 25 minutes incl. voting.
The rating:
The test is graded according to the 7-step scale with external assessment.
The test has not been passed:
The test form is the same for the re-examination