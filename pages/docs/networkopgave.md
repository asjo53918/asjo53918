#Assignments

##Assignment 1 - Fagets læringsmål

##Assignment 2 - OSI Model
### 2.1
Router - Network Layer : Moves information from Source to Destination.

Server - Physical Layer : Data Lives here and moves via cable to cable connection

Switch - Data Link : The switch is concerned with the local network, instead routing information between local clients.

Bridge - Data Link : The bridge connects two Local Area Networks so they can communicate. (En Bridge forbinder to LAN så det kan kommunikere.)

###2.2
R15 Eagle AI Router - Network Layer - A router with the features one expects of a router. Additionally, the router has a Wi-Fi Optimizer which makes many things about the router run better and more optimally.


Dlink.net(2021) (https://in.dlink.com/en/latest-news/2021/12/d-link-introduces-ai-enabled-route)

PCMag.com "A Wi-Fi signal that stops short" https://uk.pcmag.com/wireless-routers/142404/d-link-ax1500-smart-router-r15

##Assignment 3 - Fejlfinding
1. Choose a problem, preferably when it arises, alternatively at the back end. It could be printer errors, wifi connection issues, websites that are 'gone', slow internet, certificate errors, etc and describe the problem. Be aware of the difference between what is observed and what is a "spinal response".

- Today when I was updating my GitLab pages to try and document what I was working on, I ran into a failure in my pipeline; I continued to get pipeline failures regardless of what I tried.

2. Describe what you think the problem is (ie formulate a hypothesis)

- I think that when I created my pipeline I did something incorrect, or I misunderstoon a direction. I need to reread the documentation and try and find where I went wrong.

3. Find information that supports/disproves the idea (ie. look in log files, do tests, check LEDs, ...) and write it down

- I reread the documentation and checked it against my gitlab repository. Just to be sure, I attempted to run the pipeline again and it failed. After doing all that I thought to check the pipeline errors and immediately found my error.

4. Decide what can be done to remedy or mitigate the error should it occur again.

- I need to be very careful when I am making changes to the Gitlab and double check the changes I make, this last problem was caused by an accidental punktum.

5. Implement it if resources allow, or describe how it should be handled for next time.

- If there is a next time, I will start at the error code at the pipeline before checking the entire library.

6. BQ: How did you handle reaching the limit of your knowledge and/or understanding of the problem or sub-element?

- Although it was frustrating it was a good excuse to go back to StackOverflow to see if anyone had had my error previously.


##Assignment 4 - Specs
- Find the list from ww06, where we made a list of hardware and their place in the OSI model.
- Select 2-3 different devices from the list
1. Cisco Managed Switch
2. Netgear Unmanaged Switch
3. Aruba Wireless Bridge
- Find the specifications of the device
1. [Cisco Managed Switch Datasheet](https://www.cisco.com/c/en/us/products/collateral/switches/catalyst-1000-series-switches/nb-06-cat1k-ser-switch-ds-cte-en.html)
2. [NetGear Unmanaged Switch Datasheet](https://www.netgear.com/images/datasheet/switches/GS316_GS324_GS348.pdf)
3. [Aruba Wireless Bridge Datasheet](https://www.arubanetworks.com/resource/aruba-501-wireless-bridge-datasheet/)

- What is listed as restrictions? Do you think these are high numbers or low numbers?

##Assignment 5 - Internet Speed
- Go on a speed test, what is the speed?
	My speeds were 41.98 (Download) and 22.69 (Upload)
- Best guess: what is a bottleneck and why.
A bottleneck is when a lot of users simultaneously try to use the same service, which ends up with slow access for everyone. This occurs because there is a limited amount of bandwith that is available so the more users there are accessing something, the less bandwith there is to go around.
- Try possibly to run it simultaneously on two machines on the same subnet/wifi.
When I ran the speed test again with two machines, I recieved very different results. My Main PC now has a download of 15.38 and an upload of 11.38, and the Backup machine had a download of 42.79 and an upload of 11.40.

##Assignment 6 - More Internet Speed
- Install iperf
- Run Iperf against a public server, eg iperf3 -c <some server> -t 10 -i 1 -p <some port>

- Run Iperf against a public server (opposite direction), eg iperf3 -c <some server> -t 10 -i 1 -p <some port> -R
- Compare results. Were speeds as expected?
![Iperf Forward](https://gitlab.com/asjo53918/asjo53918/-/raw/main/pages/docs/images/iperf1.JPG)
![Iperf Reverse](https://gitlab.com/asjo53918/asjo53918/-/raw/main/pages/docs/images/iperf2.JPG)

My speeds were much faster going from my machine to the public server than in reverse, which was unexpected. My expectation would be speeds that were around the same.


Note: There are strict restrictions on the use of the various servers, for example that only one can use the service at a time.


- Links Used
[This](https://github.com/R0GGER/public-iperf3-servers) is a list of public iperf3 servers that are available to connect to, listed by continent and country for convenience sake.

## Assignment 7 - IPAM

1. Paper + Pencil Practice - Make an IP subnet of the associated internal and external networks.
!![A Network Diagram](https://gitlab.com/asjo53918/asjo53918/-/raw/main/pages/docs/images/Networkdiagram.png)
2. Research what Netbox is and make a list of what it can and cannot do
- Netbox is set up as a single source of truth for a specific network. It offers many features that are useful for monitoring and keeping up a network including but not limited to IP address management with full IPv4/IPv6 Parity, ASN Management, power distribution Monitoring and Virtual Machine & Cluster management.
- Netbox does not monitor networks, operate as a DNS or RADIUS server, nor does it offer configuration or facility management.
3. Set Netbox up and document your network via netbox interface.
3. Research which other methods you can make IPAM documentation in. Note the lists in your documentation.

Calico, Solarwinds, and Netris are examples of alternate IPAM Documentation programs.

There appears to be a bunch of different IPAM managers, though they don't appear to be as robust as Netbox.


---

## Week 7 . Extra Assignment

1. On a debian VM install ntopng
2. Show the connection (Default localhost port is 3000(127.0.0.1:3000, but during installation I changed mine to :5000 for reasons))
![NTOPNG](https://gitlab.com/asjo53918/asjo53918/-/raw/main/pages/docs/images/E-2.JPG)
3. Generate more traffic by visiting sites.
It is interesting to see the differences in data recieved from different types of websites. For example in my testing, Reddit is displaying many times more applications and using much more data than wikipedia, for example, due to the ad and picture heavy nature (I assume) comparitively to Wikipedia, which has a very minimalist design.

---

## Week 8 Not-An-Assignment
I have skipped over detailing installing VMWare and Kali Live because I believe it is not useful for the purposes of reporting on this project.

### Subnets

In the first part of this assignment the objective was to create two subnets to connect a network and show how information travels.

The parameters were as follows
 1. A host only network with no DHCP service, subnet address 192.168.111.0 connected to a host virtual adapter.
 2. A host only network with no DHCP service, subnet address 192.168.112.0 connected to a host virtual adapter.
 3. A NAT subnet with DHCP enabled.

![Subnet 1](https://gitlab.com/asjo53918/asjo53918/-/raw/main/pages/docs/images/subnet1.JPG)

![Subnet 2](https://gitlab.com/asjo53918/asjo53918/-/raw/main/pages/docs/images/subnet2.JPG)

The two pictures show that all subnets are set up exactly as they are supposed to be.

---
### Router Import
The next step in the assignment is to get a network set up. There were multiple steps to follow and these are also documented. The directions were as follows.

1. Download / Import the .ova file
2. Configure the subnet settings for the virtual machine made from the .ova to connect to VMNets 8,11, and 12.
3. Configure a Kali Linux machine to VMnet11
4. Start the machines up, check the IP and routing and verify that there is internet access
5. Documentation


The VMNets, as can be seen from the pictures below, show that the two Virtual Machines subnets were set up correctly as per the directions.

![Network Subnets 1](https://gitlab.com/asjo53918/asjo53918/-/raw/main/pages/docs/images/network1.jpg)

![Network Subnets 2](https://gitlab.com/asjo53918/asjo53918/-/raw/main/pages/docs/images/network2.JPG)

The two virtual machines are then spooled up and, with the use of the ipconfig and ping commands it can be shown that the Linux machine has internet access, can route through the server and has a valid IP.

![Ping](https://gitlab.com/asjo53918/asjo53918/-/raw/main/pages/docs/images/ping.JPG)

![IPconfig](https://gitlab.com/asjo53918/asjo53918/-/raw/main/pages/docs/images/2-6.JPG)

---
### Extra Virtual Machine
The last step in the assignment was to make another virtual machine and connect it to another subnet (Vmnet12) and see if it could be reached by the first computer. If all machines were set up correctly it would be possible. The directions were as follows.
1. Create a new VM and connect it to VMNet 12.
2. Ping one VM from the other and use Wireshark to document that no NAT'ing has occured between the two VM's
3. Documentation.

This was not much more work since the previous steps were working well which is documented in the following Wireshark image.

![Wireshark](https://gitlab.com/asjo53918/asjo53918/-/raw/main/pages/docs/images/ping2.JPG)

## Assignment 8 - Network Diagram

1. The above diagram must be made neatly - ie. find a suitable diagram tool and make your own version of it.
2. Add some associated text that describes what you see and the important elements
3. Find 3 questions for the diagram and take them with you to the next lesson - It's ok to have found the answer in advance, then you just have to write down questions + answers (incl. references).

![Networking Diagram](https://gitlab.com/asjo53918/asjo53918/-/raw/main/pages/docs/images/netmap.png "Netmap")

---

## Assignment 9 - Protocols

This assignment is used to train in the use of wireshark and to help recognize protocols.

# Instructions
1. Find two or three protocols from wireshark's test packages that you recognize.

![Wireshark](https://gitlab.com/asjo53918/asjo53918/-/raw/main/pages/docs/images/wireshark.JPG "Wireshark Protocols")
The protocols that I recognize from the test package I downloaded are TCP (Transmission Control Protocol), HTTP (Hypertext Transfer Protocol) and UDP (User Datagram Protocol). There are two that I didn't recognize, which I will list and look into and detail now. I hadn't heard of Gnutella - (A protocol for file sharing (mostly Music) that was initially planned to utilize GNU protocol but then didn't instead.) and I couldn't remember what ICMP (Internet Control Message Protocol) was but when I looked more into it it immedietly made more sense. It is used by networking devices to send error messages and operational information regarding success or failure when communicating between IP addresses. It differs from TCP and UDP in that it is not typically used to exchange data between systems.
 
2. Understand what is occuring, with the help of Google if necessary.
The presence of Gnutella leads me to believe that this is both a pretty old test package (As far as I can tell, Gnutella peaked in popularity in 2007). Research indicates that Gnutella utilized both UDP and TCP for different functions, UDP for moving files and TCP for. It is most likely a torrent file.

3. Take notes to share in the next class.

# Links Used
- Wiki.Wireshark.org/samplecaptures  I found my sample for Assignment 9 from here. (FTPv6-1.cap)
- https://www.gnu.org/philosophy/gnutella.html (Information about Gnutella from GNU)
---

## Assignment 10 - Traffic Analysis

# Instructions
1. Start Wireshark on a local or company network and collect some data.
2. Save PCAPS (Packet Capture)
3. Examine PCAPS. Are there any unknown protocols, Unknown IP Addresses or anything else interesting?
4. Dive into 2 or 3 streams and try to understand what is going on.
5. Take notes to share in the next class.

---

## - SSL Analysis

# Instructions
1. Install ssldump in kali or similar linux box
2. From the command line run 'ssldump -j -ANH -n -i -any | jq
3. Generate some HTTPS Traffic by browsing
4. Evaluate what is shown.
How does ssl dump know domain names? Can that be turned off?
When handshaking, the certificate chain is shown. Should that be secret?

---

## Assignment 24 - UDP Amplification Attack

###Information

UDP amplification attack is a way of doing DoS (denial of service) attacks.

###Instructions

1. Briefly explain how UDP works and why IP spoofing can work with UDP.
	UDP works by sending packets with no regard for whether they make it to the destination or not. IP Spoofing works by the attacker sending the address of an authorized system.
2. Make an example
	![UDP Spoofing](https://gitlab.com/asjo53918/asjo53918/-/raw/main/pages/docs/images/25-2.png)
3. Briefly explain what a denial of service attack is
	A DOS is a cyber attack designed to render a service inaccessable. 
4. Create an example with DNS or NTP that shows a DoS attack
	In a DNS amplification attack, an attacker spoofs the source IP address of a DNS query and sends it to open DNS resolvers on the internet. The DNS resolver then responds to the spoofed IP address with a much larger response than the original query, amplifying the amount of traffic that is sent to the victim's network.
	![DNS Attack](https://gitlab.com/asjo53918/asjo53918/-/raw/main/pages/docs/images/25-3.png)
5. What "service" is being denied?
	Access to the server is being denied, in that when a server is being accessed by a bunch of users many of them are turned away, at least temporarily.
6. All systems have bottlenecks, come up with relevant examples in this context
	Online sales come to mind, where in the first hours that they are available a web page might be unresponsive because of the amount of users attempting to see see what is on sale.
