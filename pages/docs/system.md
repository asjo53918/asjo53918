#System security

#Learning Objectives

##Knowledge
The student has knowledge of:

- General governance principles / security procedures
- Essential forensic processes
- Relevant IT threats
- Relevant security principles for system security
- OS roles in relation to security considerations
- Security administration in DBMS.

##Skills
The student can:

- Leverage countermeasures to secure systems
- Follow a benchmark to ensure the setup of the devices
- Implement systematic logging and monitoring of devices
- Analyze logs for incidents and follow an audit trail
- Can restore systems after an incident.

##Skills
The student can:

- Manage devices at the command line level
- Manage tools to identify and remove/mitigate different types of endpoint threats
- Manage the selection, application and implementation of
- Practical mechanisms to prevent, detect and respond to specific
- IT security incidents.
- Manage encryption relevant measures


#Examination
ECTS:
The scope of the exam is 10 ECTS.
Sample form:
The test consists of a written submission (individually or in a group of up to three students) followed by an individual oral exam.
The written submission must be a maximum of 10 normal pages in addition to appendices. With two students, the maximum number is 15 normal pages. With three students, the maximum number is 20 normal pages.
A normal page is 2,400 characters incl. spaces and footnotes. Front page, table of contents, bibliography and appendices are not included here. Attachments are outside of evaluation and cannot be expected to be read.
For further details, see the section on "Requirements for written tests and projects". The oral test is individual and lasts 25 minutes incl. voting
The rating:
The test is graded according to the 7-step scale by an internal assessment. According to the Examinations Order, an individual assessment is always carried out.
The assessment is an overall assessment of the written and oral performance. There is no requirement for individualization of the written group work
The test has not been passed:
The test form is the same for the re-examination
It is possible to submit a new written assignment.