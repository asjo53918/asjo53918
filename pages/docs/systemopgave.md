#Assignments

##Assignment 1

###Viden 
Den studerende har viden om: 
• Generelle governance principper / sikkerhedsprocedurer → ISO27…
• Væsentlige forensic processer → Blue teaming til identifikation af angreb
• Relevante it-trusler → angreb på systemer
• Relevante sikkerhedsprincipper til systemsikkerhed  → ???
• OS roller ift. sikkerhedsovervejelser → Authorization
• Sikkerhedsadministration i DBMS. → Credentials i databaser

###Færdigheder 
Den studerende kan: 
• Udnytte modforanstaltninger til sikring af systemer → Firewalls, honeypot, sikring imod kendte trusler
• Følge et benchmark til at sikre opsætning af enhederne → basis sikkerhedsniveau til systemer?
• Implementere systematisk logning og monitering af enheder → overvågning og eventlogs
• Analysere logs for incidents og følge et revisionsspor → kunne analysere hvordan og hvorfor et angreb er sket.
• Kan genoprette systemer efter en hændelse.  → Backup, system restoration med minimal tab af data osv.

###Kompetencer 
Den studerende kan: 
• håndtere enheder på command line-niveau → kunne bevæge sig rundt uden en GUI, derfra håndtere en server
• håndtere værktøjer til at identificere og fjerne/afbøde forskellige typer af endpoint trusler  → endpoint og API sikkerhed som en helhed?
• Håndtere udvælgelse, anvendelse og implementering af praktiske mekanismer til at forhindre, detektere og reagere over for specifikke it-sikkerhedsmæssige hændelser. → have nok viden om systemsikkerhed til at kunne udvælge det bedste hardware og software til et specifikt system
• håndtere relevante krypteringstiltag → hashing/salting, implementering af kryptering indenfor systemer.