#Assignments

##Assignment 1
---
Assignment 1's tasks are as follows

- [x] Write 3 Headlines
- [x] Create a Link
- [x] Add a picture
- [ ] Make a 4*4 table with the first column left aligned, columns 2 and 3 centered and column 4 right aligned.
- [x] Make an unordered list (bullet points) with 3 levels
- [x] Make an ordered list with 2 levels
- [x] Make a Bold Text
- [x] Make an Italics Text
- [x] Make a Bold AND Italics text
- [x] Create a code block and specify the language in which the code is written.
- [ ]
- [ ]
---
# This is a big test
## Just a normal test
### Smol Test
---
[My Gitlab!](https://gitlab.com/asjo53918/asjo53918/activity)

---
![Woah](https://cdn1.1800flowers.com/wcsstore/Flowers/images/catalog/160019mv24x.jpg?width=545&height=597&quality=80&auto=webp&optimize={medium})

---
| T  | E  | S  | T  |
|:-- |:-: |:-: |--: |
|What A long Sentence  |  ACK ACK ACK| IT  | TEST  |
| I know!  | ACK  | ITITITITITITITIT  | TEEEEEEEEEEEST  |
---

---
- Steak
- Duck
- Fish
---
1. Duck
2. Steak
---
**Bold** of you to *assume* that I didn't ***already*** know how markdown worked!

---
```
{
  "firstName": "John",
  "lastName": "Smith",
  "age": 25
}
```
---
The Cheat Sheet that was utilized in this assignment can be found [here](https://www.markdownguide.org/cheat-sheet), and if more details are needed a great guide can be found [here](https://www.markdownguide.org/getting-started). I had a bit of trouble with the markdown table but I found a solution [here](https://www.pluralsight.com/guides/working-tables-github-markdown). That writeup helped me figure out the last thing I was having trouble with.

---
##Assignment 28
#Assignment 28 - CIA Model

Instructions:

1. Read about the CIA Model in "IT-Sikkerhed i Praksis"
2. Choose **1** of the following scenario to evaluate in relation to the CIA Model
- Password Manager (Software)
- Doctors Clinic (Corporation)
- Your data on the computer and the cloud
- Energy Supplier (Critical Infrastructure)

We chose Laegeklinik.
- Confidentiality: Doctors Offices have a lot of personal medical data that they need to perform their job. It is important that this data is kept safe from attacks.
- Integrity: The same information needs to be secure also because if someone gains access and can change it then a lot of bad can be done 
- Availability : If a clinic doesn't have access to its systems then it cannot function correctly. 

3. Assess, prioritize the scenario in relation to the CIA model. Make notes and justify your choices and considerations.

- Availability: It is very important for a lægeklinik to have access to their data. They cannot do their job without it, and the medical field is one where one cannot just 'Wing it'
- Confidentility: The main danger of a confidentiality breach is that one could then falsify perscriptions.
- Integrity: The integrity of a lægekliniks data is very important, but it is the least likely to be targeted, insofar as it is unlikely that one will illegally gain access to a system to change information. That is the reasoning for it to be the least important on this list.

4. Which type(s) of hacker attack will be the most relevant to take precautions against? 
- A DDOS attack can take down a server which, depending on when it hits, can be devestating to a business
- A phishing attack gone well is an opening into a system that can be used to steal information or gain access to a system. It can also lead into a ransomware attack, which is another whole problem.

5. What precautions can you take to maintain CIA in your scenario (encryption, access control, hashing, logging etc.)

- Managing access to the System
- Awareness 
- Encrypting Data
- Logging of information in a physical way

6. Each group presents what they have come up with and everyone gives feedback.

